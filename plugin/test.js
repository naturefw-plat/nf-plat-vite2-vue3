export default function myPlugin () {
  const virtualFileId = '@vite-plugin-vue-test'

  return {
    name: 'vite-plugin-vue-test', // 必须的，将会在 warning 和 error 中显示
    resolveId (id) {
      if (id === virtualFileId) {
        return virtualFileId
      }
    },
    load (id) {
      if (id === virtualFileId) {
        // return 'export const msg1 = "from virtual file22222222222"'
        return `export const msg1 = () => {
          return "222222"
        } `
      }
    }
  }
}
