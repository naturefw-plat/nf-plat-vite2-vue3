import { watch } from 'vue'
import { ElMessage } from 'element-plus'
// eslint-disable-next-line import/no-absolute-path
import service from '/services'

/**
 * 表单组件的共用代码
 * @param {proxy} props 组件的属性
 * @param {object} model 表单控件的v-model
 * @returns 三个函数：
 * * watch props.formMetaId
 * * watch props.dataId
 * * mysubmit
 */
export default function formManage (props, model) {
  /**
   * 根据组件属性的 dataId 加载需要的数据
   */
  const changeFormDataId = (formProps, isAdd) => {
    // 监听记录ID的变化，加载数据便于修改
    watch(() => props.dataId, (id) => {
      switch (props.type) {
        case 'add':
          break
        case 'update':
          // 加载数据
          console.log('获取要修改的数据', id)
          service(props.moduleId, 40, {}, id).then((data) => {
            Object.assign(model, data[0])
            formProps.events.reset()
          })
          break
        case 'mod':
          // 加载数据
          service(props.moduleId, 40, {}, id).then((data) => {
            console.log('mod', data)
            if (data.length === 0) {
              isAdd.value = true
            } else {
              isAdd.value = false
              Object.assign(model, data[0])
              formProps.events.reset()
            }
          })
          break
        default:
          break
      }
    },
    { immediate: true })
  }

  /**
   * 提交表单数据
   * @param {*} dataListState 列表状态
   */
  const mySubmit = (dataListState, isAdd) => {
    let msg = ''
    let reload = () => {}
    switch (props.type) {
      case 'add':
        msg = '添加数据成功！'
        reload = dataListState.reloadFirstPager
        break
      case 'update':
        msg = '修改数据成功！'
        reload = dataListState.reloadCurrentPager
        break
      case 'mod':
        reload = dataListState.reloadCurrentPager
        //  if (isAdd) {
        break
    }
    service(props.moduleId, props.actionId, model, props.dataId)
      .then(() => {
        ElMessage({
          type: 'success',
          message: msg
        })
        // 更新数据列表
        reload()
      })
  }

  return {
    changeFormDataId,
    mySubmit
  }
}
