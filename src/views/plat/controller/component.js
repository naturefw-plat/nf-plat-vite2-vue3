import { defineAsyncComponent } from 'vue'

/**
 * 注册动态组件
 */
export default {
  'plat-baseset': defineAsyncComponent(() => import('../plat-baseset/index.vue')),
  master: defineAsyncComponent(() => import('../base-page/list-form.vue')),
  list: defineAsyncComponent(() => import('../base-page/list.vue')),
  mod: defineAsyncComponent(() => import('../base-page/form.vue')),
  form: defineAsyncComponent(() => import('../base-page/form.vue'))
}
