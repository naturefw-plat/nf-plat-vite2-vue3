import { watch, reactive } from 'vue'
// 状态
// eslint-disable-next-line import/no-absolute-path
import VueDS from '/nf-state'

// 调用服务
// eslint-disable-next-line import/no-absolute-path
import service from '/services'

/**
 * * 数据列表的通用管理类
 * * 注册列表的状态
 * * 关联获取数据的方式
 * * 设置快捷键
 * @param {string} modeluId 模块ID
 * @returns 列表状态管理类
 */
export default function dataListControl (modeluId) {
  // 显示数据列表的数组
  const dataList = reactive([])

  // 访问状态
  const { reg, get } = VueDS.useStore()
  // 子组件里面获取父组件注册的状态
  // const dataListState = get.dataListState()
  // const dataListState = get.dataListState('list_' + modeluId)

  // 数据加载中
  let isLoading = false

  /**
   * 父组件注册状态
   * @returns 注册列表状态
   */
  const regDataListState = () => {
    // 注册列表的状态，用于分页、查询、添加、修改、删除等'list_' + modeluId
    const state = reg.dataListState()

    //  重新加载第一页，统计总数（添加、查询后）
    state.reloadFirstPager = () => {
      isLoading = true
      state.pager.pagerIndex = 1 // 显示第一页
      // state.pager.pageTotal = 0 // 需要统计总记录数
      if (Object.keys(state.fixedQuery).length > 0) {
        Object.assign(state.query, state.fixedQuery)
      }
      // 获取数据
      const pagerInfo = {
        query: state.query,
        pager: state.pager,
        useCount: true
      }
      service(modeluId, 50, pagerInfo, true).then((data) => {
        state.pager.pagerTotal = data.pager.pagerTotal
        dataList.length = 0
        dataList.push(...data.list)
        isLoading = false
      })
    }
    // 先执行一下，获取初始数据
    state.reloadFirstPager()

    // 重新加载当前页，不统计总数（修改后）
    state.reloadCurrentPager = () => {
      if (Object.keys(state.fixedQuery).length > 0) {
        Object.assign(state.query, state.fixedQuery)
      }
      // 获取数据
      service(modeluId, 50, state).then((data) => {
        dataList.length = 0
        dataList.push(...data.list)
      })
    }

    // 重新加载当前页，统计总数（删除后）
    state.reloadPager = () => {
      // state.pager.pageTotal = 0 // 需要统计总记录数
      isLoading = true
      if (Object.keys(state.fixedQuery).length > 0) {
        Object.assign(state.query, state.fixedQuery)
      }
      // 获取数据
      const pagerInfo = {
        query: state.query,
        pager: state.pager,
        useCount: true
      }
      service(modeluId, 50, pagerInfo).then((data) => {
        state.pager.pagerTotal = data.pager.pagerTotal
        dataList.length = 0
        dataList.push(...data.list)
        isLoading = false
      })
    }

    // 监听，用于翻页控件的翻页。翻页，获取指定页号的数据
    watch(() => state.pager.pagerIndex, (v1, v2) => {
      // 避免重复加载
      if (isLoading) {
        // 删除中，不获取数据
        return
      }
      if (Object.keys(state.fixedQuery).length > 0) {
        Object.assign(state.query, state.fixedQuery)
      }
      // 获取数据
      service(modeluId, 50, state).then((data) => {
        dataList.length = 0
        dataList.push(...data.list)
      })
    })

    return state
  }

  /**
   * 列表页面的快捷键
   */
  const setHotkey = (dataListState) => {
    // 设置分页、操作按钮等快捷键
    // 计时器做一个防抖
    let timeout
    let tmpIndex = 0
    document.onkeydown = (e) => {
      // console.log('key-doc:', e)
      if (!(e.target instanceof HTMLBodyElement)) return // 表单触发，退出
      if (e.altKey) {
        // alt + 的快捷键，调用操作按钮的事件
        dataListState.hotkey(e.key)
      } else {
        // 翻页
        const maxPager = parseInt(dataListState.pager.pagerTotal / dataListState.pager.pagerSize) + 1
        switch (e.key) {
          case 'ArrowLeft': // 左箭头 上一页
          case 'PageUp':
          case 'a':
            dataListState.pager.pagerIndex -= 1
            if (dataListState.pager.pagerIndex <= 0) {
              dataListState.pager.pagerIndex = 1
            }
            break
          case 'ArrowRight': // 右箭头 下一页
          case 'PageDown':
          case 'd':
            dataListState.pager.pagerIndex += 1
            if (dataListState.pager.pagerIndex >= maxPager) {
              dataListState.pager.pagerIndex = maxPager
            }
            break
          case 'ArrowUp': // 上箭头
          case 'Home': // 首页
          case 'w':
            dataListState.pager.pagerIndex = 1
            break
          case 'ArrowDown': // 下箭头
          case 'End': // 末页
          case 's':
            dataListState.pager.pagerIndex = maxPager
            break
          default:
            // 判断是不是数字
            if (!isNaN(parseInt(e.key))) {
              // 做一个防抖
              tmpIndex = tmpIndex * 10 + parseInt(e.key)
              clearTimeout(timeout) // 清掉上一次的计时
              timeout = setTimeout(() => {
                // 修改 modelValue 属性
                if (tmpIndex === 0) {
                  dataListState.pager.pagerIndex = 10
                } else {
                  if (tmpIndex >= maxPager) {
                    tmpIndex = maxPager
                  }
                  dataListState.pager.pageIndex = tmpIndex
                }
                tmpIndex = 0
              }, 500)
            }
        }
      }

      e.stopPropagation()
    }
  }

  return {
    setHotkey, // 设置快捷键
    regDataListState, // 父组件注册状态
    dataList // 父组件获得列表
    // dataListState // 子组件获得状态
  }
}
