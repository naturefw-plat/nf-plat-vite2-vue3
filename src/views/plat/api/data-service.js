// webSQL
import { webSQLVue } from 'nf-web-storage'
// 获取配置
// eslint-disable-next-line import/no-absolute-path
import { serviceModel } from '/jsonPlatAPI/service-model.json'

/**
 * 针对实体的操作
 * * 添加一条数据
 * * 修改一条数据
 * * 删除一条数据
 * * 获得一条数据
 * @param {string} serviceId 服务ID
 */
export default function dataService (serviceId) {
  // 根据服务ID，获取列表数据

  // 访问 webSQL
  const { help } = webSQLVue.useHelp('nf-plat-meta')

  const tableName = serviceModel[serviceId].tableName
  /**
   * 获取一条记录
   * @param {string} dataId 主键字段值
   * @returns 符合条件的一条记录
   */
  const getData = (dataId) => {
    // 获取一条记录
    return new Promise((resolve, reject) => {
      help.selectOne(tableName, dataId).then((data) => {
        // 获取一条记录
        resolve(data)
      })
    })
  }

  /**
   * 添加一条记录
   * @param {object} model 主键字段值
   * @returns 新记录的ID
   */
  const addData = (model) => {
    // 添加一条记录
    return new Promise((resolve, reject) => {
      delete model.ID
      help.insert(tableName, model).then((data) => {
        resolve(data)
      })
    })
  }

  /**
   * 修改一条记录
   * @param {object} model 主键字段值
   * @param {string} dataId 主键字段值
   * @returns 涉及的记录数
   */
  const updateData = (model, dataId) => {
    // 修改一条记录
    return new Promise((resolve, reject) => {
      delete model.ID
      help.update(tableName, model, dataId).then((data) => {
        resolve(data)
      })
    })
  }

  /**
   * 删除一条记录
   * @param {string} dataId 主键字段值
   * @returns 涉及的记录数
   */
  const deleteData = (dataId) => {
    // 删除一条记录
    return new Promise((resolve, reject) => {
      help.del(tableName, dataId).then((data) => {
        resolve(data)
      })
    })
  }

  return {
    addData, // 添加一条记录
    updateData, // 修改一条记录
    deleteData, // 删除一条记录
    getData // 获取一条记录
  }
}
