// webSQL
import { webSQLVue } from 'nf-web-storage'
// 获取配置
import loadmeta from './loadmeta.js'

/**
 * 数据列表的服务
 */
export default function dataListService () {
  // 根据服务ID，获取列表数据

  // 访问 webSQL
  const { help } = webSQLVue.useHelp('nf-plat-meta')

  /**
   * 获取记录总数
   * @param {string} tableName 表名
   * @param {object} query 查询条件
   * @returns 符合条件的总记录数
   */
  const getDataCount = (tableName, query) => {
    // 统计总数
    return new Promise((resolve, reject) => {
      help.selectCount(tableName, query).then((count) => {
        // 设置分页
        resolve(count)
      })
    })
  }

  /**
   * 获取记录集合
   * @param {string} tableName 表名
   * @param {object} listCol 需要显示的字段
   * @param {object} query 查询条件
   * @param {object} pager 分页信息
   * * * query: { 100: [401, ''] }
   * * * pager: {
   * * *  * pageTotal: 100, // 0：需要统计总数；其他：不需要统计总数
   * * *  * pageSize: 2, // 一页记录数
   * * *  * pageIndex: 1, // 第几页的数据，从 1  开始
   * * *  * orderBy: { id: false } // 排序字段
   * * * }
   * @returns 符合条件的记录集合
   */
  const getDataList = (tableName, listCol, query, pager) => {
    // 获取数据
    return new Promise((resolve, reject) => {
      help.select(tableName, listCol, query, pager).then((data) => {
        resolve(Array.from(data))
      })
    })
  }

  /**
   * 依据查询条件和分页条件，获取数据列表
   * @param {*} serviceId 服务ID
   * @param {*} query 查询条件
   * @param {*} pager 分页信息
   * * * query: { 100: [401, ''] }
   * * * pager: {
   * * *  * pageTotal: 100, // 0：需要统计总数；其他：不需要统计总数
   * * *  * pageSize: 2, // 一页记录数
   * * *  * pageIndex: 1, // 第几页的数据，从 1  开始
   * * *  * orderBy: { id: false } // 排序字段
   * * * }
   */
  const loadDataList = (serviceId, pager, query = {}, isCount = false) => {
    return new Promise((resolve, reject) => {
      // 依据服务ID获取表名和字段集合
      // console.log('异步加载', serviceList)
      loadmeta(serviceId).then((data) => {
        console.log('meta---', data)
        const listInfo = data

        // 获取数据
        if (isCount) {
          // 需要统计总数
          help.selectCount(listInfo.tableName, query).then((data) => {
            // console.log('获取到的总数', data)
            const _count = data
            // 获取数据
            help.select(listInfo.tableName, listInfo.cols, query, pager).then((data) => {
              // console.log('获取到的数据列表', Array.from(data))
              resolve({
                count: _count,
                list: Array.from(data)
              })
            })
          })
        } else {
          // 不需要统计总数
          help.select(listInfo.tableName, listInfo.cols, query, pager).then((data) => {
            // console.log('获取到的数据列表111', Array.from(data))
            resolve(Array.from(data))
          })
        }
      })
    })
  }

  return {
    getDataCount, // 获取记录总数
    getDataList, // 获取记录集合
    loadDataList // 加载数据列表
  }
}
