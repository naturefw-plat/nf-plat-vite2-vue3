import axios from 'axios'
// eslint-disable-next-line import/no-absolute-path
import { menu } from '/jsonPlat/menu.json'

let url = ''
// 判断环境
switch (window.location.host) {
  case 'naturefw.gitee.io': // 演示，生产
    url = '/nf-vue-cdn/elecontrol/json-plat-api/'
    break
  case 'localhost:8200': // 测试
    url = '/json-plat-api/'
    break
  case 'localhost:3000': // 开发
    url = '/public/json-plat-api/'
    break
}

/**
 * 通过模块ID和名称加载json文件
 * * 需要自己做缓存
 */
export default function loadmeta (moduleId) {
  return new Promise((resolve, reject) => {
    const currMenu = menu.filter((a) => a.id === moduleId)[0]
    // 加载
    axios.get(`${url}${currMenu.moduleFlag}.json`).then((res) => {
      // console.log('异步加载', res)
      if (res.status === 200) { // statusText
        // 正常
        resolve(res.data)
      }
    })
  })
}
