import { defineAsyncComponent } from 'vue'

/**
 * 注册动态组件
 */
export default {
  // 主从表的页面
  // master: defineAsyncComponent(() => import('../base-page/list-form.vue')),
  // 列表页面
  list: defineAsyncComponent(() => import('../base-page/list.vue')),
  // 同时添加、修改的表单
  mod: defineAsyncComponent(() => import('../base-page/form.vue')),
  // 添加或者修改的表单
  form: defineAsyncComponent(() => import('../base-page/form.vue'))
}
