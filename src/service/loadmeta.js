import axios from 'axios'

let url = ''
// 判断环境
switch (window.location.host) {
  case 'naturefw.gitee.io': // 演示，生产
    url = '/nf-plat-vite2-vue3/'
    break
  case 'localhost:3000': // 开发
    url = '/public/'
    break
}

/**
 * 通过模块ID和名称加载json文件
 * * 需要自己做缓存
 */
export default function loadmeta (state, id) {
  return new Promise((resolve, reject) => {
    // 加载
    let url2 = url
    if (state === 'module') {
      url2 += `service/${id}.json`
    } else {
      url2 += `model/${id}.json`
    }
    axios.get(url2).then((res) => {
      // console.log('异步加载', res)
      if (res.status === 200) { // statusText
        // 正常
        resolve(res.data)
      }
    })
  })
}
