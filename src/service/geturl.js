/**
 * 根据运行环境，返回对应的url
 */

export default function getPlatURL () {
  let url = ''
  // 判断环境
  switch (window.location.host) {
    case 'naturefw.gitee.io':
      url = '/nf-plat-vite2-vue3/'
      break
    case '172.17.96.1:3000':
    case '192.168.31.148:3000':
    case 'localhost:3000':
    case 'localhost:3001':
      url = '/public/'
      break
  }

  return url
}
