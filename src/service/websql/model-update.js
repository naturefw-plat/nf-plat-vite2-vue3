/**
 * 实现修改服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} info 服务的 meta
 * @param {object} model 前端提交的 body
 * @param {number|string} id 记录ID
 * @returns 返回新添加的记录的ID
 */
export default function update (userInfo, help, info, model, id) {
  return new Promise((resolve, reject) => {
    // console.log('\n启动 update 服务\n')
    help.updateModel(info, model, id).then((count) => {
      // console.log('外部修改数据，影响行数:', count)
      resolve({ count })
    }).catch((err) => {
      reject(err) // '修改数据出错！'
    })
  })
}
