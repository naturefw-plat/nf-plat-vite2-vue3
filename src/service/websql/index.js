import modeladd from './model-add.js'
import modelupdate from './model-update.js'
import modeldelete from './model-delete.js'
import modelget from './model-get.js'
import listpager from './list-pager.js'
import modelall from './list-all.js'
import createTable from './table-create.js'

export default {
  'list-all': modelall,
  'list-pager': listpager,
  'model-delete': modeldelete,
  'model-get': modelget,
  'model-add': modeladd,
  'model-update': modelupdate
}
