
/**
 * 实现获取记录服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} info 服务的 meta
 * @param {object} pagerInfo 分页和查询信息
 * @returns 返回新添加的记录的ID
 */
export default function getPager1 (userInfo, help, info, pagerInfo) {
  return new Promise((resolve, reject) => {
    // console.log('\n启动 getPager 服务\n') // , info
    const query = pagerInfo.query
    const pager = info.pager
    pager.pagerIndex = pagerInfo.pager.pagerIndex
    // console.log('分页信息:', pager)
    help.listPager(info, query, pager, pagerInfo.useCount).then((res) => {
      // console.log('获取分页数据2:', list)
      pager.pagerTotal = res.count
      resolve(res)
    }).catch((err) => {
      reject(err) // '获取分页数据出错！'
    })
  })
}
