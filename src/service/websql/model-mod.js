// 引入help
import { addModel, updateModel } from '../../../packages/websql.js'

/**
 * 实现添加、修改服务。如果没有记录则添加；有了就修改。
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} info 服务的 meta
 * @param {object} model 前端提交的 body
 * @returns 返回新添加的记录的ID
 */
export default function add (userInfo, help, info, model, id) {
  return new Promise((resolve, reject) => {
    // console.log('\n启动 mod 服务\n')
    help.begin((cn) => {
      // 获取记录
      getModel(help, info, id, cn).then((model1) => {
        console.log('获取数据2222333:', model1)
        // 看看有没有
        if (model1.length === 0) {
          // 添加
          addModel(help, info, model, cn).then((newId) => {
            // console.log('外部添加数据:', newId)
            resolve({ newId })
          }).catch((err) => {
            reject(err) // '添加数据出错！'
          })
        } else {
          // 修改
          updateModel(help, info, model, id, cn).then((count) => {
            // console.log('外部添加数据:', newId)
            resolve({ count })
          }).catch((err) => {
            reject(err) // '修改数据出错！'
          })
        }
      }).catch((err) => {
        reject(err) // '获取一条数据出错！'
      })
    })
  })
}
