
/**
 * 实现删除服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} info 服务的 meta
 * @param {object} model 占位用
 * @param {number|string} id 记录ID
 * @returns 返回影响的行数
 */
export default function del (userInfo, help, info, model, id) {
  return new Promise((resolve, reject) => {
    // console.log('\n启动 delete 服务\n')
    help.delModel(info, id).then((count) => {
      // console.log('删除数据，影响行数:', count)
      resolve({ count })
    }).catch((err) => {
      reject(err) // '删除数据出错！'
    })
  })
}
