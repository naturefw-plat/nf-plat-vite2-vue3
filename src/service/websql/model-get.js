
/**
 * 实现获取记录服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} info 服务的 meta
 * @param {object} model 占位用
 * @param {number|string} id 记录ID
 * @returns 返回新添加的记录的ID
 */
export default function getData (userInfo, help, info, model, id) {
  return new Promise((resolve, reject) => {
    // console.log('\n启动 getOne 服务\n', info)
    help.getModel(info, id).then((model) => {
      // 添加成功
      // console.log('获取数据:', model)
      resolve(model)
    }).catch((err) => {
      reject(err) // '获取一条数据出错！'
    })
  })
}
