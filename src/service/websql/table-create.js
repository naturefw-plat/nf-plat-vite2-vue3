
/**
 * 实现添加服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} info 服务的 meta
 * @param {object} model 前端提交的 body
 * @returns 返回新添加的记录的ID
 */
export default function createTable (userInfo, help, tableName, model) {
  return new Promise((resolve, reject) => {
    // console.log('\n启动 add 服务\n')
    help.createTable(tableName, model).then((res) => {
      // console.log('外部添加数据:', newId)
      resolve(res)
    }).catch((err) => {
      reject(err) // '添加数据出错！'
    })
  })
}
