/**
 * 实现获取记录服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} info 服务的 meta
 * @param {object} query 查询条件
 * @returns 返回新添加的记录的ID
 */
export default function getAll1 (userInfo, help, info, query) {
  return new Promise((resolve, reject) => {
    // console.log('\n启动 getAll 服务\n', info)
    // console.log('查询条件:', query)

    help.listAll(info, query)
      .then((list) => {
        // 添加成功
        // console.log('获取数据列表:', list)
        resolve({ list })
      })
      .catch((err) => {
        reject(err)
      })
  })
}
