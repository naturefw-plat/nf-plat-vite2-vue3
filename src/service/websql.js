/**
 * 实现MySQL的增删改查
 */
/* eslint-disable import/no-absolute-path */
import { useSQLHelp } from 'nf-web-storage'

// 状态
import { state } from 'nf-state'

// ji
import loadMeta from './loadmeta.js'
// 操作数据库
import action from './websql/index.js'

// let help = null

const actions = {
  'list-all': 'listAll',
  'list-pager': 'listPager',
  'model-delete': 'delModel',
  'model-get': 'getModel',
  'model-add': 'addModel',
  'model-update': 'updateModel'
}
/**
 * 实现前端的webSQL的增删改查
 * @param {string|number} moduleId 模块ID
 * @param {string|number} actionId 动作ID
 * @param {string|number} dataId 记录ID
 * @param {object} json model或者其他
 */
export default function websql (moduleId = null, actionId, dataId, json, flag = null) {
  // const dbFlag = state.dbFlag
  const serviceMeta = state.projectMeta.service
  // 通用：操作数据库
  const help = state.project_date_sqlHelp
  // console.log('服务的meta------------', serviceMeta)

  const modelKey = serviceMeta[moduleId].actions[actionId].model
  const modelKind = serviceMeta[moduleId].actions[actionId].kind
  const meta = serviceMeta[moduleId].models[modelKey]
  // console.log('服务的meta------------', modelKey, modelKind, meta)

  // console.log('controller获取 help---webSQL---', help)
  // 加载模块信息
  // 加载动作信息
  // 处理
  return new Promise((resolve, reject) => {
    action[modelKind]({}, help, meta, json, dataId)
      .then((res) => {
        resolve(res)
      })
      .catch((err) => {
        reject(err) // '添加数据出错！'
      })
    /*
    loadMeta('module', moduleId).then((res) => {
      const moduleMeta = res
      const actionMeta = res.actions[actionId]
      // console.log('获取模块信息：', moduleId, moduleMeta)
      // 根据动作ID，调用处理函数
      loadMeta('action', actionMeta.model).then((res) => {
        const meta = res
        // console.log('获取模块信息：', moduleId, moduleMeta)
        // console.log('获取动作信息：', actionId, meta)
        action[actionMeta.kind]({}, help, meta, json, dataId)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err) // '添加数据出错！'
          })
      })
    })
    */
  })
}
