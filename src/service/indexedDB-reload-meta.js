/**
 * 重新加载指定的meta，便于更新json文件
 */

// 新 indexedDB
import { useDBHelp } from 'nf-web-storage'

// 轻量级状态
// eslint-disable-next-line import/no-absolute-path
import VueDS from 'nf-state'
// axios
import axios from 'axios'
// eslint-disable-next-line import/no-absolute-path
import { metaList } from '/jsonPlat/loadModule.json'
// 获取json文件的url
import getUrl from './geturl.js'
const url = getUrl()
let help = {}
let state = {}
/**
 * 根据模块ID获取模块名称
 * @param {number|string} moduleId 模块ID
 */
function getModuleName (moduleId) {
  let re = {}
  Object.keys(metaList).forEach(key => {
    if (key.split('-')[0] === moduleId.toString()) {
      re = {
        key,
        forms: metaList[key]
      }
    }
  })

  return re
}

/**
 * 生成加载json文件用 axios集合
 * @param {object} moduleName 模块名称和表单名称
 * @returns axios集合
 */
function getModuleMeta (moduleName) {
  // 加载json的 请求的 数组，交给 Promise.all 使用
  const loadModuleMeta = [
    axios.get(`${url}${moduleName.key}/button.json?v=${Math.random()}`),
    axios.get(`${url}${moduleName.key}/find.json?v=${Math.random()}`),
    axios.get(`${url}${moduleName.key}/grid.json?v=${Math.random()}`)
  ]
  // 添加表单
  moduleName.forms.forEach(form => {
    const _url = `${url}${moduleName.key}/${form}.json?v=${Math.random()}`
    loadModuleMeta.push(axios.get(_url))
  })

  return loadModuleMeta
}

/**
 * 依据模块ID，重新加载对应的json文件（一套），
 * 然后存入indexedDB 并且返回
 * @param {number} moduleId 模块ID
 * @returns 返回模块需要的meta
 */
export default function reloadMeta (moduleId = null) {
  if (moduleId === null) {
    state = VueDS.useStore().state
    help = useDBHelp(state.dbFlag.project_db_meta)
    return
  }
  // 加载json 存入indexedDB，返回meta
  return new Promise((resolve, reject) => {
    // 获取加载json的url
    const moduleName = getModuleName(moduleId)
    const metaName = typeof state.platMeta === 'undefined' ? 'projectMeta' : 'platMeta'
    // 加载json的 请求的 数组，交给 Promise.all 使用
    const loadModuleMeta = getModuleMeta(moduleName)

    console.log('axios 开始加载json：', window.performance.now())
    Promise.all(loadModuleMeta).then((res) => {
      // console.log('data:', res)
      console.log('axios 加载json完毕：', window.performance.now())
      if (res[0].status === 200) { // statusText
        // 存入indexedDB
        const modulesMeta = {
          moduleId: moduleId,
          button: res[0].data,
          find: res[1].data,
          grid: res[2].data,
          forms: {}
        }
        // 添加 forms
        for (let i = 3; i < res.length; i++) {
          const form = res[i].data
          modulesMeta.forms[form.formId] = form
          // 写入缓存
          Object.assign(state[metaName][moduleId].forms[form.formId], form)
        }
        // 写入缓存，这样可以保持响应性
        Object.assign(state[metaName][moduleId].grid, modulesMeta.grid)
        Object.assign(state[metaName][moduleId].find, modulesMeta.find)
        Object.assign(state[metaName][moduleId].button, modulesMeta.button)

        // 写入 indexedDB
        console.log('indexedDB 开始修改数据：', window.performance.now(), res)
        help.putModel('moduleMeta', modulesMeta, moduleId).then((res) => {
          console.log('indexedDB 修改数据完成：', window.performance.now(), res)
        })
        // 返回meta
        resolve(modulesMeta)
      }
    })
  })
}
