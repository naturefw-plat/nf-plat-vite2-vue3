/**
 * 判断访问后端API，还是使用前端 websql
 */

import axios from 'axios'
import websql from './websql.js'

/**
 * MVC 的 controller。
 * 判断访问后端API，还是使用前端 websql
 * @param {*} moduleId 模块ID
 * @param {*} actionId 动作ID
 * @param {*} json 提交的数据
 * @param {*} dataId 记录ID
 */
export default function server (moduleId = null, actionId, json = null, dataId = -2, flag = null) {
  const state = 'online'
  return new Promise((resolve, reject) => {
    if (state === 'online') {
      // 使用 webSQL
      websql(moduleId, actionId, dataId, json, flag)
        .then((res) => {
          // MySQL返回数据
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        })
    } else {
      // 使用后端API
      const url = `/api/${moduleId}/${actionId}/${dataId}`
      if (json === null) {
        axios.get(url)
          .then((res) => {
            // 后端API返回数据
            resolve(res.data)
          })
          .catch((err) => {
            reject(err)
          })
      } else {
        axios.post(url, json)
          .then((res) => {
            // 后端API返回数据
            resolve(res.data)
          })
          .catch((err) => {
            reject(err)
          })
      }
    }
  })
}
