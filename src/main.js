import { createApp } from 'vue'
import App from './App.vue'
// import store from './store-plat2/state-plat.js' // 轻量级状态
import store from './store-plat/state.js' // 轻量级状态

// UI库
import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'
import 'dayjs/locale/zh-cn'
import locale from 'element-plus/lib/locale/lang/zh-cn'

// 拖拽弹窗
import dialogDrag from './control-web/js/dialogDrag.js'
// 拖拽table的th
import tableDrag from './control-web/visual-drag/tableDrag.js'
// 获取table的一些信息
import tableGet from './control-web/js/tableManage.js'
// 拖拽 form
import formDrag from './control-web/js/formDrag.js'

// 管理平台的meta
import platMetaHelp from './store-plat/plat-meta-controller.js'
console.log('main.js')

// 执行初始化
platMetaHelp()

/**
 * 通用
 * 从db获取客户项目 的 meta === 共用方式，meta -》 组件
 * 管理客户项目的 SQL data === 共用方式，项目数据库
 * 特殊
 * 编辑客户项目的 SQL、db 的 meta === 特殊
 */

// 加载数据库连接信息
// import { platMetaDB } from '../public/db-config.json'

// 平台状态的管理
// import PlatController from './store-plat2/controller.js'

// 获取 meta 供程序使用。支撑平台 meta 的 缓存
// import dbPlatMeta from './store-plat2/plat-meta-db.js'

// 维护客户项目 的 meta，(webSQL)
// import sqlProjectMeta from './store-project/project-meta-sql.js'

// 客户项目 meta 的 缓存，可以直接更新项目的 meta 的缓存。
// import dbProjectMeta from './store-project/project-meta-db2.js'

// sql-data 维护客户项目 的 data，存放客户数据，建表用。
// import sqlProjectData from './store-project/project-data-sql.js'

// 开始设置
// const platStateManage = new PlatController()
// console.log('platStateManage', platStateManage)

// 设置 indexedDB（平台meta），完成后通知 状态管理
// dbPlatMeta(platMetaDB, (help) => {
//   platStateManage.dbReady(help)
// })

// 设置 webSQL，完成后通知 状态管理
// sqlProjectMeta((help) => {
// stateManage.sqlReady(help)
// })

// sqlProjectData()

const app = createApp(App)
app.config.devtools = true
app.config.productionTip = true

app.use(store) // 简易状态
  .use(dialogDrag) // 对话框的拖拽
  .use(tableDrag) // table的拖拽
  .use(tableGet) // 获取table的一些信息
  .use(formDrag) // form 的拖拽
  .use(ElementPlus, { locale, size: 'small' }) // UI库
  .mount('#app')
