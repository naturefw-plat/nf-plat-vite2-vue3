/**
 * indexedDB 的演示
 */
/* eslint-disable import/no-absolute-path */
// 访问 indexedDB
import { dbCreateHelp } from 'nf-web-storage'

// 引入数据库数据
const db = {
  dbName: 'nf-plat-meta',
  ver: 1
}

/**
 * 客户项目的 meta 的 db 缓存
 */
export default function createDBHelp (callback) {
  const help = dbCreateHelp({
    dbFlag: 'plat-meta-db',
    dbConfig: db,
    stores: { // 数据库里的表
      moduleMeta: { // 模块的meta {按钮,列表,分页,查询,表单若干}
        id: 'moduleId',
        index: {},
        isClear: false
      },
      menuMeta: { // 菜单用的meta
        id: 'id',
        index: {},
        isClear: false
      },
      serviceMeta: { // 后端API的meta，在线演示用。
        id: 'moduleId',
        index: {},
        isClear: false
      }
    },
    // 设置初始数据
    async init (help) {
      if (typeof callback === 'function') {
        await callback(help)
      }
    }
  })
  return help
}
