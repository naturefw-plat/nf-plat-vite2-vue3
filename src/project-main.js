import { createApp } from 'vue'
import App from './project-App.vue'
import store from './store-project/state.js' // 轻量级状态

// UI库
import ElementPlus from 'element-plus'
// import 'element-plus/lib/theme-chalk/index.css'
import 'dayjs/locale/zh-cn'
import locale from 'element-plus/lib/locale/lang/zh-cn'

// 拖拽弹窗
import dialogDrag from './control-web/js/dialogDrag.js'
// 拖拽table的th
import tableDrag from './control-web/visual-drag/tableDrag.js'
// 获取table的一些信息
import tableGet from './control-web/js/tableManage.js'
// 拖拽 form
import formDrag from './control-web/js/formDrag.js'

// 项目的meta
import { platMeta, projectMeta } from './store-project/project-meta-controller.js'

platMeta()
projectMeta()

const app = createApp(App)
app.config.devtools = true
app.config.productionTip = true

app.use(store) // 简易状态
  .use(dialogDrag) // 对话框的拖拽
  .use(tableDrag) // table的拖拽
  .use(tableGet) // 获取table的一些信息
  .use(formDrag) // form 的拖拽
  .use(ElementPlus, { locale, size: 'small' }) // UI库
  .mount('#app')
