/* eslint-disable import/no-absolute-path */
import { reactive, provide, watch, inject, onUpdated, onBeforeUnmount } from 'vue'
// 调用服务，访问 webSQL 或者 MySQL
import service from '/services'

// provide 的标识
const __flagList = Symbol('nf-state-list')

/**
 * 列表页面的状态的管理
 */
export default function stateListManage (_flag = '这个家伙很懒，没有设置flag') {
  // 列表功能需要的状态
  const state = reactive({
    query: {}, // 用户输入的查询条件
    fixedQuery: {}, // 固定的查询条件，比如 isdel = 0
    pager: { // 分页参数
      pagerTotal: 100, // 0：需要统计总数；其他：不需要统计总数
      pagerSize: 5, // 一页记录数
      pagerIndex: 1, // 第几页的数据，从 1  开始
      orderBy: { id: false } // 排序字段
    },
    choice: { // 列表里面选择的记录
      dataId: '', // 单选，便于修改和删除
      dataIds: [], // 多选，便于批量删除
      row: {}, // 选择的记录数据（单选），仅限于列表里面的。
      rows: [] // 选择的记录数据（多选），仅限于列表里面的。
    },
    hotkey: () => {}, // 处理快捷键的事件，用于操作按钮
    reloadFirstPager: () => {}, // 重新加载第一页，统计总数（添加后）
    reloadCurrentPager: () => {}, // 重新加载当前页，不统计总数（修改后）
    reloadPager: () => {} // 重新加载当前页，统计总数（删除后）

  })

  // 列表的数据源
  const dataListSource = reactive([])
  // 数据加载中
  let isLoading = false

  /**
   * 设置功能
   */
  const setFunction = (moduleId) => {
    //  重新加载第一页，统计总数（添加、查询后）
    state.reloadFirstPager = () => {
      isLoading = true
      state.pager.pagerIndex = 1 // 显示第一页
      // state.pager.pageTotal = 0 // 需要统计总记录数
      if (Object.keys(state.fixedQuery).length > 0) {
        Object.assign(state.query, state.fixedQuery)
      }
      // 获取数据
      const pagerInfo = {
        query: state.query,
        pager: state.pager,
        useCount: 0
      }
      service(moduleId, 50, pagerInfo, state.pager.pagerTotal).then((data) => {
        state.pager.pagerTotal = data.count
        dataListSource.length = 0
        dataListSource.push(...data.list)
        isLoading = false
      })
    }
    // 先执行一下，获取初始数据
    state.reloadFirstPager()

    // 重新加载当前页，不统计总数（修改后）
    state.reloadCurrentPager = () => {
      if (Object.keys(state.fixedQuery).length > 0) {
        Object.assign(state.query, state.fixedQuery)
      }
      // 获取数据
      service(moduleId, 50, state).then((data) => {
        dataListSource.length = 0
        dataListSource.push(...data.list)
      })
    }

    // 重新加载当前页，统计总数（删除后）
    state.reloadPager = () => {
      // state.pager.pageTotal = 0 // 需要统计总记录数
      isLoading = true
      if (Object.keys(state.fixedQuery).length > 0) {
        Object.assign(state.query, state.fixedQuery)
      }
      // 获取数据
      const pagerInfo = {
        query: state.query,
        pager: state.pager,
        useCount: true
      }
      service(moduleId, 50, pagerInfo).then((data) => {
        state.pager.pagerTotal = data.count
        dataListSource.length = 0
        dataListSource.push(...data.list)
        isLoading = false
      })
    }
  }

  const setHotkey = () => {
  }

  /**
   * 设置监听
   */
  const setWatch = (moduleId) => {
    // 监听，用于翻页控件的翻页。翻页，获取指定页号的数据
    watch(() => state.pager.pagerIndex, (v1, v2) => {
      // 避免重复加载
      if (isLoading) {
        // 删除中，不获取数据
        return
      }
      if (Object.keys(state.fixedQuery).length > 0) {
        Object.assign(state.query, state.fixedQuery)
      }
      // 获取数据
      service(moduleId, 50, state).then((data) => {
        dataListSource.length = 0
        dataListSource.push(...data.list)
      })
    })

    onUpdated((msg) => {
      console.log('updated', msg)
    })

    onBeforeUnmount((msg) => {
      console.log('beforeUnmount', msg)
    })
  }

  /**
   * 父组件注册状态
   * @returns 注册列表状态
   */
  const regState = (moduleId) => {
    // 初始设置
    setFunction(moduleId)
    setWatch(moduleId)
    provide(__flagList, state)
    return state
  }

  /**
   * 子组件获取状态
   * @returns 注册列表状态
   */
  const getState = () => {
    const _state = inject(__flagList)
    if (typeof _state === 'undefined') {
      // 没有找到状态
      return null
    } else {
      // 找到了，返回
      return _state
    }
  }

  return {
    setHotkey, // 设置快捷键
    dataListSource,
    regState,
    getState
  }
}
