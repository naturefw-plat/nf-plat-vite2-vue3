/* eslint-disable import/no-absolute-path */
/**
 * 数据列表的状态管理
 */

import { reactive, provide, inject, watch } from 'vue'
// 调用服务，访问 webSQL 或者 MySQL
import service from '/services'

const _flagList = Symbol('___nf-state-list_')

/**
 * 数据列表的状态的controller
 */
class DataList {
  constructor (moduleId) {
    this.moduleId = moduleId
    this.dataListSource = reactive([]) // 数据源
    this.query = reactive({}) // 查询条件
    this.fixedQuery = reactive({}) // 固定的查询条件，比如 isdel = 0
    this.pager = reactive({ // 分页参数
      pagerTotal: 100, // 0：需要统计总数；其他：不需要统计总数
      pagerSize: 5, // 一页记录数
      pagerIndex: 1, // 第几页的数据，从 1  开始
      orderBy: { id: false } // 排序字段
    })
    this.choice = reactive({ // 列表里面选择的记录
      dataId: '', // 单选，便于修改和删除
      dataIds: [], // 多选，便于批量删除
      row: {}, // 选择的记录数据（单选），仅限于列表里面的。
      rows: [] // 选择的记录数据（多选），仅限于列表里面的。
    })
    this.isLoading = false // 避免重复加载
  }

  // 处理快捷键的事件，用于操作按钮
  hotkey () {

  }

  /**
   * 设置监听，翻页时自动获取记录
   */
  setWatch () {
    // 监听，用于翻页控件的翻页。翻页，获取指定页号的数据
    watch(() => this.pager.pagerIndex, (v1, v2) => {
      // 避免重复加载
      if (this.isLoading) {
        // 删除中，不获取数据
        return
      }
      if (Object.keys(this.fixedQuery).length > 0) {
        Object.assign(this.query, this.fixedQuery)
      }
      // 获取数据
      const pagerInfo = {
        query: this.query,
        pager: this.pager,
        useCount: 0
      }
      service(this.moduleId, 50, pagerInfo).then((data) => {
        this.dataListSource.length = 0
        this.dataListSource.push(...data.list)
      })
    })
  }

  // 重新加载第一页，统计总数（添加后）
  reloadFirstPager () {
    this.isLoading = true
    this.pager.pagerIndex = 1 // 显示第一页
    // this.pager.pageTotal = 0 // 需要统计总记录数
    if (Object.keys(this.fixedQuery).length > 0) {
      Object.assign(this.query, this.fixedQuery)
    }
    // 获取数据
    const pagerInfo = {
      query: this.query,
      pager: this.pager,
      useCount: 0
    }
    service(this.moduleId, 50, pagerInfo).then((data) => {
      this.pager.pagerTotal = data.count
      this.dataListSource.length = 0
      this.dataListSource.push(...data.list)
      this.isLoading = false
    })
  }

  // 重新加载当前页，不统计总数（修改后）
  reloadCurrentPager () {
    if (Object.keys(this.fixedQuery).length > 0) {
      Object.assign(this.query, this.fixedQuery)
    }
    const pagerInfo = {
      query: this.query,
      pager: this.pager,
      useCount: 0
    }
    // 获取数据
    service(this.moduleId, 50, pagerInfo).then((data) => {
      this.dataListSource.length = 0
      this.dataListSource.push(...data.list)
    })
  }

  // 重新加载当前页，统计总数（删除后）
  reloadPager () {
    // this.pager.pageTotal = 0 // 需要统计总记录数
    this.isLoading = true
    if (Object.keys(this.fixedQuery).length > 0) {
      Object.assign(this.query, this.fixedQuery)
    }
    // 获取数据
    const pagerInfo = {
      query: this.query,
      pager: this.pager,
      useCount: 0
    }
    service(this.moduleId, 50, pagerInfo).then((data) => {
      this.pager.pagerTotal = data.count
      this.dataListSource.length = 0
      this.dataListSource.push(...data.list)
      this.isLoading = false
    })
  }

  // 初始化
  init () {
    // 初始设置，获得数据
  }
}

// 注册
const regList = (moduleId) => {
  const ds = new DataList(moduleId)
  ds.setWatch()
  provide(_flagList, ds) //  + moduleId
  return ds
}

/**
 * 子组件获取状态
 * @returns 注册列表状态
 */
const getList = (moduleId) => {
  const ds = inject(_flagList) //  + moduleId
  if (typeof ds === 'undefined') {
    // 没有找到状态
    return null
  } else {
    // 找到了，返回
    return ds
  }
}

export {
  DataList,
  regList,
  getList
}
