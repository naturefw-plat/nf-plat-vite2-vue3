/* eslint-disable import/no-absolute-path */
/**
 * 动态tab的状态
 */

import { reactive, provide, watch, inject, onUpdated, onBeforeUnmount } from 'vue'
// 全局状态，获取菜单
import { state } from 'nf-state'

console.log('新状态', state)

// provide 动态 tab 的标识
const __flagTab = Symbol('nf-state-tab')
// 做一个简单的路由
const location = window.location

// 点击菜单触发的 hash 变化
let isMenuSetHash = true

export default function stateTabManage (_flag = '这个家伙很懒，没有设置flag') {
  // 列表功能需要的状态
  const _state = reactive({
    trigger: '', // 记录触发者，菜单触发，要恢复初始状态；tab触发，保持状态。
    tabData: [], // 存放 tab信息 的数组，渲染tab用
    activeModuleId: 0, // 当前激活的 menu/tab 的 id
    openModudle: (moduleId) => {}, // 单击菜单后打开模块对应的tab，更新状态
    tabClick: (tab) => {}, // 单击tab后激活对应的tab，保持状态
    tabRemove: (moduleId, index) => {} // 关闭tab的事件
  })

  const setFunction = () => {
    /**
     * 打开/新建一个tab
     */
    _state.openModudle = (moduleId) => {
      _state.trigger = 'menu'
      _state.activeModuleId = parseInt(moduleId)
      location.hash = '#' + _state.activeModuleId
    }
    /**
     * 单击tab的事件，切换tab
     * @param {*} tab el-tabs组件 tab-click 事件传递的参数
     */
    _state.tabClick = (tab) => {
      console.log('tabClick', tab)
      const moduleId = parseInt(tab.props.name)
      // 设置状态
      _state.trigger = 'tab'
      _state.activeModuleId = moduleId
    }

    /**
     * 关闭 tab
     * @param {*} moduleId 模块ID
     * @param {*} index tabData 的序号
     */
    _state.tabRemove = (moduleId, index) => {
      // console.log('tabRemove', moduleId)
      // 找到要删除 tab 的下标
      // 判断是不是激活的tab。非激活，直接删除；激活的，删除后找到左面的tab
      if (moduleId === _state.activeModuleId) {
        if (_state.length > 1) {
          // 删除激活 tab，激活左侧标签
          _state.activeModuleId = _state.tabData[index - 1].id
        } else {
          // 激活桌面
          _state.activeModuleId = 0
        }
        location.hash = '#' + _state.activeModuleId
      }
      _state.tabData.splice(index, 1)
    }
  }

  /**
   * 初始化的时候看看url的 hash，创建对应的tab
   */
  const menuSetup = () => {
    // 如果有 hash ，自动创建一个 tab
    const _moduleId = location.hash.replace('#', '')
    if (_moduleId !== '' && !isNaN(_moduleId)) {
      setTimeout(() => {
        _state.trigger = 'menuFrist'
        _state.activeModuleId = parseInt(_moduleId)
      }, 500)
    }
  }

  /**
   * 设置监听
   */
  const setWatch = () => {
    // 监听 url 的 hash 变化，好像没啥用
    window.onhashchange = (arg) => {
      console.log('----- window 监听 hash change', arg)
      const hash = location.hash
      console.log('----- 得到的hash：', hash)
      if (isMenuSetHash) {
        // 菜单触发
        console.log('----- 菜单触发的hash：', hash)
        isMenuSetHash = false
      } else {
        // url地址栏 触发
        console.log('----- url地址栏 触发的hash：', hash)
        _state.trigger = 'url11'
        _state.activeModuleId = parseInt(hash.replace('#', ''))
      }
    }

    // 监听 单击的菜单，即模块ID
    watch(() => _state.activeModuleId, (moduleId) => {
      console.log('监听的 moduleId ', moduleId)
      // if (Object.keys(param).length === 0) return

      // 判断是否已经生成标签
      if (_state.tabData.findIndex((item) => item.id === moduleId) === -1) {
        // 没有标签，创建新标签
        // 获取当前菜单的数据
        if (typeof state.projectMeta.menu.length !== 'undefined') {
          const m = state.projectMeta.menu.filter(a => a.id === moduleId)[0]
          if (typeof m !== 'undefined') {
            _state.tabData.push({
              id: moduleId, // 模块ID
              componentKind: m.componentKind, // 组件字典类型
              title: m.title // 标签
            })
          }
        }
      }
    },
    { immediate: true })
  }

  /**
   * 父组件注册状态
   * @returns 注册列表状态
   */
  const regState = (moduleId) => {
    // 初始设置
    setFunction(moduleId)
    setWatch(moduleId)
    menuSetup()
    provide(__flagTab, _state)
    return _state
  }

  /**
   * 子组件获取状态
   * @returns 注册列表状态
   */
  const getState = () => {
    const _state = inject(__flagTab)
    if (typeof _state === 'undefined') {
      // 没有找到状态
      return null
    } else {
      // 找到了，返回
      return _state
    }
  }

  return {
    regState,
    getState
  }
}
