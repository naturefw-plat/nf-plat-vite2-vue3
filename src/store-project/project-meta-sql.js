/**
 * project-meta-sql
 * 项目的webSQL的meta，开发的时候设置meta用
 */
/* eslint-disable import/no-absolute-path */

// 引入 webSQL 的 help
import { sqlCreateHelp } from 'nf-web-storage'

const db = {
  title: '项目运行需要的 meta(webSQL)',
  dbName: 'nf-project-meta',
  ver: 1.0,
  remarks: '客户项目需要的元数据，即meta，开发模式使用。',
  size: 10
}

/**
 * 客户项目的meta，仿后端MySQL
 */
export default function setup (callback) {
  const install = sqlCreateHelp({
    dbFlag: 'project-meta-sql',
    dbConfig: db,
    tables: { // meta需要的表
      // 表
      nf_table: {
        tableId: '', // 表, int
        tableName: '', // 表名, varchar
        content: '' // 表说明, varchar
      },
      // 字段
      nf_table_columns: {
        columnId: '', // 字段标识, int
        tableId: '', // 表ID, int
        columnKind: '', // 字段类型, int
        colName: '', // 字段名称, varchar
        cnName: '', // 对外名称, varchar
        colType: '', // 字段类型, varchar
        colSize: '', // 字段大小, int
        defaultValue: '', // 默认值, varchar
        canNull: '', // 允许空, int
        controlType: ''
      },
      // 模块
      nf_module: {
        moduleId: '', // 模块ID, int
        parentId: '', // 父级ID, int
        parentIdAll: '', // 父级ID集合, varchar
        moduleLevel: '', // 模块层级, tinyint
        powerMark: '', // 权限标识, varchar
        title: '', // 模块名称, varchar
        icon: '', // 图标, varchar
        componentKind: '', // 组件字典名称, varchar
        routerName: '', // 路由名称, varchar
        routerParam: '', // 路由参数, varchar
        isLeaf: '', // 是否叶节点, bit
        isHidden: '', // 是否隐藏, tinyint
        isLock: '' // 是否锁定, bit
      },
      v_module_button: {
        buttonId: '', // 按钮ID, int
        moduleId: '', // 所在模块, int
        btnTitle: '', // 按钮名称, varchar
        btnKind: '', // 按钮类型, varchar
        dialogTitle: '', // 弹窗名称, varchar
        dialogWidth: '', // 弹窗宽度, varchar
        controlKey: '', // 组件名称, varchar
        openModuleId: '', // 表单ID, int
        formMetaId: '', // 表单的meta,
        actionId: '', // actionId, int
        hotkey: '', // 快捷键, varchar
        disOrder: '' // 排序, int
      },
      // 模块的列表的分页
      v_module_pager: {
        moduleId: '', // 分页ID, int
        showCols: '', // 显示字段, 数组
        query: '', // 查询条件, 对象
        pageSize: '', // 一页记录数, int
        pageTotal: '', // 总记录数, int
        pageIndex: '', // 第几页, int
        OrderColumns: '', // 排序字段, 对象
        fixedQuery: '', // 固定查询条件, 对象
        fristQuery: '' // 首次查询条件, 对象
      },
      // 模块的列表
      v_module_grid: {
        moduleId: '', // 列表ID, int
        idName: '', // 主键字段名, varhcar
        foreignIdName: '', // 外键字段名, varhcar
        height: '', // 高度, int
        colOrder: '', // 显示字段, 数组
        stripe: '', // 斑马纹, bit
        border: '', // 纵向边框, bit
        fit: '', // 是否自撑开, bit
        highlightCurrentRow: '', // 要高亮当前行, bit
        currentRowKey: '', // 当前行的 key, varhcar
        itemMeta: '' // 子组件属性, 对象
      },
      v_module_grid_item: {
        itemId: '', // 列表字段ID, int
        moduleId: '', // 模块ID, varhcar
        columnId: '', // 字段ID, int
        colName: '', // 字段英文名
        label: '', // 标签, varhcar
        width: '', // 宽度, int
        title: '', // 标题, varhcar
        align: '', // 标题对齐, tinyint
        headerAlign: '' // 内容对齐, tinyint
      },
      // 查询
      v_module_find: {
        moduleId: '', // 查询ID, int
        quickFind: '', // 快捷查询, 数组
        allFind: '', // 全部查询, 数组
        labelWidth: '', // label宽度, int
        finditemWidth: '', // 查询项宽度, int
        customer: '', // 自定义查询, 对象
        customerDefault: '', // 默认查询方案, int
        findKind: '', // 查询方式, 对象
        itemMeta: '' // 子组件属性, 对象
      },
      v_module_find_item: {
        moduleId: '', // 查询ID, int
        columnId: '', // 字段ID
        colName: '', // 字段英文名
        label: '', // 字段中文名
        controlType: '', // 控件类型
        defValue: '', // 默认值
        placeholder: '', // 占位符
        title: '', // 浮动提示
        extend: '' // 扩展属性
      },
      // 表单
      v_module_form: {
        formId: '', // 表单ID, int
        moduleId: '', // 模块ID, int
        formColCount: '', // 列数, int
        colOrder: '', // 字段ID集合, 数组
        formColShow: '', // 组件联动, 对象
        ruleMeta: '', // 验证规则, 对象
        itemMeta: '' // 子组件属性, 对象
      },
      v_module_form_item: {
        moduleId: '', // 模块ID, int
        formId: '', // 表单ID, int
        columnId: '', // 字段ID
        colName: '', // 字段英文名
        label: '', // 字段中文名
        controlType: '', // 控件类型
        defValue: '', // 默认值
        placeholder: '', // 占位符
        title: '', // 浮动提示
        extend: '' // 扩展属性
      },
      // 后端API的meta，在线演示用
      v_service: {
        serviceId: '', // 模块ID, int
        serviceName: '' // 服务名称
      },
      v_service_action: {
        actionId: '', // actionID, int
        serviceId: '', // 模块ID, int
        actionName: '', // 名称
        kind: '', // 操作方式
        modelId: '' // 使用的modelID
      },
      v_service_model: {
        modelId: '', // modelID int
        serviceId: '', // 服务ID int
        modelName: '', // model名称 varhcar
        tableNameId: '', // 表ID int
        tableName: '', // 表名 varhcar
        idKeyId: '', // 字段ID int
        idKey: '', // 主键名称 varhcar
        colIds: '', // 字段ID集合，便于修改字段名
        cols: '', // 需要的字段ID集合 数组
        pagerSize: '', // 一页记录数 int
        orderBy: '' // 排序字段 varhcar
      }
    },
    isDeleteOldTable: false, // 是否删除之前的表
    // 可以给全局状态设置初始状态，同步数据可以直接在上面设置，如果是异步数据，可以在这里设置。
    init (help, tables) {
      if (typeof callback === 'function') {
        callback(help, tables)
      }
    }
  })
  return install
}
