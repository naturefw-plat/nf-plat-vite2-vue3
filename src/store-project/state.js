/* eslint-disable import/no-absolute-path */
// 加载状态的类库
import { createStore } from 'nf-state'

export default createStore({
  // 读写状态，直接使用 reactive
  state: {
    // 用户是否登录以及登录状态
    user: {
      isLogin: false,
      name: 'jyk', //
      age: 19
    },
    platMeta: { // 平台meta，绑定支撑平台，绑定维护控件
      menu: [],
      module: {},
      service: {}
    },
    projectMeta: { // 项目meta，绑定客户项目
      menu: [],
      module: {},
      service: {}
    },
    project_meta_dbHelp: {}, // 开发时维护meta
    project_meta_sqlHelp: {}, // 开发时维护meta
    project_date_sqlHelp: {} // 平台：项目meta；项目：客户数据
  },
  // 全局常量，使用 readonly
  readonly: {
    // json文件的文件夹名称，用于axios的url。
    dbFlag: {
      plat_meta_folder: 'json-plat',
      project_meta_folder: 'json-project',
      project_db_meta: 'plat-meta-db' // 平台 运行时需要的 meta。
    }
  },
  // 跟踪状态，用 proxy 给 reactive 套娃
  track: {
    trackTest: {
      name: '跟踪测试',
      age: 18,
      children1: {
        name1: '子属性测试',
        children2: {
          name2: '再嵌一套'
        }
      }
    },
    test2: {
      name: ''
    }
  },
  // 可以给全局状态设置初始状态，同步数据可以直接在上面设置，如果是异步数据，可以在这里设置。
  init (state, read) {
  }
})
