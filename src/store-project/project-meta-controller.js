/**
 * 客户项目 meta 的管理
 */
/* eslint-disable import/no-absolute-path */

// meta 的 help
import { MetaHelp } from 'nf-meta'

// 获取 meta 的url
import getMetaUrl from '../service/geturl.js'

// 获取状态
import { state } from 'nf-state'

// 支撑平台的 meta ，用 indexedDB 做缓存
import createDBHelpPlat from '../store-plat/plat-meta-db.js'

// 平台要维护的客户项目用的 meta，用 webSQL存放
import createSQLHelpProject from './project-meta-sql.js'

// 客户项目的 meta ，用 indexedDB 做缓存，开发用
import createDBHelpProject from './project-meta-db2.js'

// 客户项目的 data，webSQL，存放客户项目的数据
import createSQLHelpData from './project-data-sql.js'

const xiuzheng = (_meta) => {
  // 手动修正一下
  const tmpMeta = {
    menu: _meta.menu,
    module: {},
    service: {}
  }
  for (const key in _meta.module) {
    const m = _meta.module[key]
    tmpMeta.module[m.moduleId] = m
  }
  for (const key in _meta.service) {
    const s = _meta.service[key]
    tmpMeta.service[s.moduleId] = s
  }
  return tmpMeta
}

/**
 * 平台 meta
 * 从 json 加载 meta，存入 indexedDB
 * * 从webSQL加载 meta ，存入 indexedDB，然后存入 meta
 * @param {*} folder 加载 json 的文件夹
 * @param {*} useWebSQL 是否使用webSQL加载
 * @returns
 */
const platMeta = () => {
  const folder = state.dbFlag.plat_meta_folder
  const jsonUrl = getMetaUrl() + folder + '/'

  const metaHelp = new MetaHelp(jsonUrl)

  // 创建 支撑平台用的 indexedDB 数据库，完成后触发回调
  const dbHelpPlat = createDBHelpPlat(async (help) => {
    // indexedDB 准备好了
    console.log('== indexedDB 创建完毕 help：', help, '检查是否有记录------')
    // 设置indexedDB 的help
    metaHelp.dbHelp = help

    // 判断有没有数据，有数据，加如状态；没有数据，加载数据，然后在加入状态
    const count = await help.getCount('menuMeta')
    console.log('menuMeta 的 数量：', count)
    if (count === 0) {
      // 没有记录，加载
      metaHelp.loagMetaFromJson().then((meta) => {
        console.log('dbReady 的回调 ：', meta)
        const tmpMeta = xiuzheng(meta)
        metaHelp.toState(state.platMeta, tmpMeta)
      })
    } else {
      // 有记录了， 从 indexedDB 里面加载状态
      const _meta = await metaHelp.loadMetaFromDB()
      console.log('有记录的加载 ：', _meta)
      // Object.assign(state.platMeta, _meta)
      const tmpMeta = xiuzheng(_meta)
      console.log('调整后 ：', tmpMeta)

      metaHelp.toState(state.platMeta, tmpMeta)
    }
  })
}

/**
 * 客户项目 meta
 * 从 json 加载 meta，存入 indexedDB
 * * 从webSQL加载 meta ，存入 indexedDB，然后存入 meta
 * @param {*} folder 加载 json 的文件夹
 * @param {*} useWebSQL 是否使用webSQL加载
 * @returns
 */
const projectMeta = () => {
  const folder = state.dbFlag.project_meta_folder
  const jsonUrl = getMetaUrl() + folder + '/'

  const metaHelp = new MetaHelp(jsonUrl)

  // dbhelp 和SQLhelp都准备好之后调用的函数
  const sqlToDB = async () => {
    if (metaHelp.dbHelp !== null && metaHelp.sqlHelp !== null) {
      const meta = await metaHelp.sqlToDB()
      const tmpMeta = xiuzheng(meta)
      metaHelp.toState(state.projectMeta, tmpMeta)
    }
  }

  // 创建 支撑平台用的 indexedDB 数据库，完成后触发回调
  const platHelp = createDBHelpProject(async (help) => {
    // indexedDB 准备好了
    console.log('== indexedDB 创建完毕 【项目 meta】 的 help：', help, '检查是否有记录------')
    // 设置indexedDB 的help
    metaHelp.dbHelp = help

    // 存入状态
    state.project_meta_dbHelp = help

    // 判断有没有数据，有数据，加如状态；没有数据，加载数据，然后在加入状态
    const count = await help.getCount('menuMeta')
    console.log('menuMeta 的 数量 == 【项目】 menuMeta：', count)
    if (count === 0) {
      // 没有记录，加载
      metaHelp.loagMetaFromJson().then((meta) => {
        console.log('dbReady 的回调 == 【项目】 meta ：', meta)
        const tmpMeta = xiuzheng(meta)
        state.platMeta = tmpMeta
        metaHelp.toState(state.projectMeta, tmpMeta)
        // 调用SQL
        sqlToDB()
      })
    } else {
      // 有记录了， 从 indexedDB 里面加载状态
      const meta = await metaHelp.loadMetaFromDB()
      console.log('有记录的加载 == 【项目】 meta ：', meta)
      const tmpMeta = xiuzheng(meta)
      metaHelp.toState(state.projectMeta, tmpMeta)
      // 调用SQL
      sqlToDB()
    }
  })

  /**
   * 打开客户项目的 meta， webSQL
   */
  const sqlHelpProjectMeta = createSQLHelpProject(async (help) => {
    state.project_meta_sqlHelp = help
    metaHelp.sqlHelp = help
    console.log('打开客户项目的 meta， webSQL help ：', help)
    // 调用SQL
    sqlToDB()
  })

  /**
   * 打开客户项目的 data ， webSQL
   */
  const sqlHelpProjectData = createSQLHelpData(async (help) => {
    state.project_date_sqlHelp = help
    console.log('打开客户项目的 data， webSQL help ：', help)
  })

  return {
    platHelp,
    projectMeta,
    sqlHelpProjectMeta,
    sqlHelpProjectData

  }
}

export {
  platMeta,
  projectMeta
}
