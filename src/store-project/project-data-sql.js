/**
 * project-data-sql
 * 客户项目用的 数据库 -- webSQL ，增删改查的数据，或者建表用。
 */
/* eslint-disable import/no-absolute-path */

// 引入 webSQL 的 help
import { sqlCreateHelp } from 'nf-web-storage'

const db = {
  title: '客户项目的数据库(webSQL)',
  dbName: 'nf-project-data',
  ver: 1.0,
  remarks: '演示用的客户数据库',
  size: 10
}

/**
 * 客户数据库，客户项目的数据库，仿后端MySQL
 * dbFlag: 'sql-data'：便于项目代码的统一
 */
export default function setup (callback) {
  const install = sqlCreateHelp({
    dbFlag: 'project-data-sql',
    dbConfig: db,
    tables: { // 数据库里的表
      r_role: {
        id: '序号',
        roleId: '角色编号',
        roleName: '角色名称',
        remark: '角色介绍',
        roleKind: '角色类型',
        canUse: '是否可以使用'
      }
    },
    isDeleteOldTable: false, // 是否删除之前的表
    // 可以给全局状态设置初始状态，同步数据可以直接在上面设置，如果是异步数据，可以在这里设置。
    init (help) {
      // 可以做一些事情，比如添加初始数据
      const data = { // 对象属性，建立表的依据
        roleId: '1',
        roleName: '测试',
        roleKind: 1,
        canUse: 1
      }
      // const sql = 'insert into r_role (roleId, roleName, remark) VALUES (?,?,?)'
      for (let i = 0; i < 2; i++) {
        // help.query(sql, [i, i, i])
      }
    }
  })
  return install
}
