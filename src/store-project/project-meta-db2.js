/* eslint-disable import/no-absolute-path */
/**
 * public-meta-db
 * 项目运行需要的 meta，从json加载，以及从webSQL加载（开发阶段）
 * 客户项目的 meta 的缓存
 */
/* eslint-disable import/no-absolute-path */

// 新 indexedDB
import { dbCreateHelp } from 'nf-web-storage'

// 引入数据库数据
const db = {
  dbName: 'nf-project-meta',
  ver: 2
}

/**
 * 客户项目的 meta 的 db 缓存
 */
export default function setup (callback) {
  const install = dbCreateHelp({
    dbFlag: 'project-meta-db',
    dbConfig: db,
    stores: { // 数据库里的表
      moduleMeta: { // 模块的meta {按钮,列表,分页,查询,表单若干}
        id: 'moduleId',
        index: {},
        isClear: false
      },
      menuMeta: { // 菜单用的meta
        id: 'id',
        index: {},
        isClear: false
      },
      serviceMeta: { // 后端API的meta，在线演示用。
        id: 'moduleId',
        index: {},
        isClear: false
      }
    },
    // 加入初始数据
    init (help) {
      if (typeof callback === 'function') {
        callback(help)
      }
    }
  })
  return install
}
