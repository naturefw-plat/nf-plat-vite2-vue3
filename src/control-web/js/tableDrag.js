
/**
 * 拖拽 table 的 th，返回拖拽信息
 */
const tableDrag = (app, options) => {
  app.directive('tabledrag', {
    // 指令的定义
    mounted (el, binding) {
      // console.log('===== el', el)
      // console.log('===== binding', binding)

      /**
       * 实现 th 的拖拽
       * @param {string} className 用于找到目标的 class 名称。
       * @param {reactive} dragInfo reactive 返回拖拽信息。
       * @returns 没有返回
       * * const dragInfo = {
       * *  offsetX: 0,
       * *  isLeft: true, // th 左侧结束拖拽
       * *  ctrl: false, // 是否按下ctrl
       * *  source: '', // 开始拖拽的th
       * *  target: '', // 结束拖拽的th
       * *  sourceIndex: 0, // 开始拖拽的序号
       * *  targetIndex: 0 // 结束拖拽的序号
       * * })
       */
      const setThforDrag = (className, dragInfo) => {
        const table = el.getElementsByClassName(className)[0]
        const tr = table.rows[0]
        const tdCount = tr.cells.length
        // 记录 th 的序号和宽度
        const thIndex = {}
        // 记录临时的源
        let src1 = ''
        let src2 = 1
        // 设置th的拖拽
        for (let i = 0; i < tdCount; i++) {
          const th = tr.cells[i]
          thIndex[th.innerText] = {
            index: i, // 记录th的序号
            width: th.offsetWidth // 记录 th 的宽度
          }
          // 设置可以拖拽
          th.setAttribute('draggable', true)
          // 拖拽时经过
          // th.ondragover = null
          th.ondragover = (event) => {
            event.preventDefault()
          }
          // 开始拖拽
          // th.ondragstart = null
          th.ondragstart = (event) => {
            // console.log('ondragstart - event', event)
            src1 = event.target.innerText
            src2 = thIndex[event.target.innerText].index
          }
          // 结束拖拽
          // th.ondrop = null
          th.ondrop = (event) => {
            // console.log('ondrop - event', event)
            dragInfo.offsetX = event.offsetX
            dragInfo.ctrl = event.ctrlKey
            dragInfo.source = src1
            dragInfo.sourceIndex = src2
            dragInfo.target = event.target.innerText
            // console.log('ondrop - dragInfo', dragInfo)
            // 寻找th的序号
            dragInfo.targetIndex = thIndex[event.target.innerText].index
            dragInfo.isLeft = dragInfo.offsetX < thIndex[event.target.innerText].width / 2
          }
        }
      }

      binding.value.setThforDrag = setThforDrag
    }
  })
}

export default tableDrag
