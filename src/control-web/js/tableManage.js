
// element-plus 的 table，获取td的宽度等操作
const tableGet = (app, options) => {
  app.directive('tableget', {
    // 指令的定义
    mounted (el, binding) {
      // console.log('===== el', el)
      // console.log('===== binding', binding)

      binding.value.getTdwidth = () => {
        // console.log('getTd ===== el', el)
        const table = el.getElementsByClassName('el-table__header')[0]
        const tr = table.rows[0]
        // td 数量
        const tdCount = tr.cells.length
        // td 的宽度
        const tdsWitdh = []
        for (let i = 0; i < tdCount; i++) {
          const tdWidth = tr.cells[i].offsetWidth
          tdsWitdh.push(tdWidth)
        }
        return tdsWitdh
      }
    }
  })
}

export default tableGet
