
// element-plus 的 dialog 的拖拽方法
const dialogDrag = (app, options) => {
  app.directive('dialogdrag', {
    // 指令的定义
    mounted (el, binding) {
      // console.log('===  dialogDrag == binding', binding)
      // binding.arg
      // binding.value
      // 可视窗口的宽度
      const clientWidth = document.documentElement.clientWidth
      // 可视窗口的高度
      const clientHeight = document.documentElement.clientHeight

      // 弹窗的容器
      const domDrag = el.firstElementChild.firstElementChild
      // 拖拽的div
      const domMove = el.firstElementChild.firstElementChild.firstElementChild

      // 记录坐标
      // console.log('el', el)
      // 计算分数
      let tmpWidth = binding.value.replace('%', '') //
      tmpWidth = clientWidth * (100 - tmpWidth) / 200

      const domset = {
        x: tmpWidth,
        y: clientHeight * 15 / 100 // 根据 15vh 计算
      }

      // 重新设置上、左距离
      domDrag.style.marginTop = domset.y + 'px'
      domDrag.style.marginLeft = domset.x + 'px'

      // 记录拖拽开始的光标坐标，0 表示没有拖拽
      const start = { x: 0, y: 0 }
      // 移动中记录偏移量
      const move = { x: 0, y: 0 }

      // 经过时改变鼠标指针形状
      domMove.onmouseover = (e) => {
        domMove.style.cursor = 'move' // 改变光标形状
      }

      // 鼠标按下，开始拖拽
      domMove.onmousedown = (e) => {
        // 判断对话框是否重新打开
        if (domDrag.style.marginTop === '15vh') {
          // 重新打开，设置 domset.y  top
          domset.y = clientHeight * 15 / 100
        }
        start.x = e.clientX
        start.y = e.clientY
        domMove.style.cursor = 'move' // 改变光标形状
      }

      // 鼠标移动，实时跟踪 domMove
      domMove.onmousemove = (e) => {
        if (start.x === 0) { // 不是拖拽状态
          return
        }
        move.x = e.clientX - start.x
        move.y = e.clientY - start.y

        // console.log('鼠标over', start, end)
        // 初始位置 + 拖拽距离
        domDrag.style.marginLeft = (domset.x + move.x) + 'px'
        domDrag.style.marginTop = (domset.y + move.y) + 'px'
      }
      // 鼠标抬起，结束拖拽 domMove
      domMove.onmouseup = (e) => {
        move.x = e.clientX - start.x
        move.y = e.clientY - start.y

        // 记录新坐标，作为下次拖拽的初始位置
        domset.x += move.x
        domset.y += move.y
        domMove.style.cursor = ''
        domDrag.style.marginLeft = domset.x + 'px'
        domDrag.style.marginTop = domset.y + 'px'
        // 结束拖拽
        start.x = 0

        // window.document.onmousemove = null
        // window.document.onmouseup = null
      }
    }
  })
}

export default dialogDrag
