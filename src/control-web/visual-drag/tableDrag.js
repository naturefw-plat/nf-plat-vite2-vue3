import { nextTick, reactive } from 'vue'

// 加载 gird 的管理函数
import gridManage from './drag-m-grid.js'

/**
 * * 1. 拖拽 table 的 th，调用设置函数
 * * 2. 拖拽 表单的 label，调用设置函数
 * * 3. 拖拽 查询的 label，调用设置函数
 */
const tableDrag = (app, options) => {
  app.directive('tabledrag', {
    // 指令的定义
    mounted (el, binding) {
      // console.log('===== el', el)
      // console.log('===== binding', binding)

      // 控件的类型：grid、form、find
      const metaKind = binding.value.metaKind
      // 控件的meta
      const meta = binding.value.meta

      nextTick(() => {
        // console.log('下一轮1：', el.getElementsByClassName('el-table__header').length)
        // 这时候可以得到 th，但是没有 body 部分
        // 获取管理函数
        switch (metaKind) {
          case 'grid':
            gridManage(el, meta).girdSetup()
            break
          case 'form':
            break
          case 'find':
            break
        }
      })
    }
  })
}

export default tableDrag
