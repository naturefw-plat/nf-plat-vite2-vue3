
/**
 * 设置 dom 拖拽事件
 * @param {dom} dom 可以拖拽的 dom
 * @param {*} textToColumnID dom 的 innerText 和字段ID的对应关系
 * @param {*} dragInfo 拖拽信息
 * @param {*} meta 元数据，好像这里不需要
 */
const setDrag = (dom, textToColumnId, dragInfo, dragEvent, meta, el) => {
  // 设置 dom 可以拖拽
  dom.setAttribute('draggable', true)
  // 拖拽时经过
  dom.ondragover = null // 去掉以前加载的事件
  dom.ondragover = (event) => {
    event.preventDefault()
    clearTimeout(dragInfo.timeout)
    dragInfo.timeout = setTimeout(() => {
      if (dragInfo.state !== 'end') {
        console.log('拖拽出去了。------', dragInfo)
        dragEvent.outEnd(dragInfo) // 拖拽到另一个 dom，释放
      }
    }, 1000)
  }

  // 开始拖拽
  dom.ondragstart = null // 去掉以前加载的事件
  dom.ondragstart = (event) => {
    // console.log('ondragstart - event', event)
    dragInfo.state = 'pending'
    dragInfo.sourceId = textToColumnId[event.target.innerText]
    dragInfo.sourceIndex = meta.colOrder.findIndex(a => a === dragInfo.sourceId)
  }

  // 结束拖拽
  dom.ondrop = null // 去掉以前加载的事件
  dom.ondrop = (event) => {
    event.preventDefault()
    // console.log('ondrop - event', event)
    dragInfo.state = 'end'
    dragInfo.offsetX = event.offsetX
    dragInfo.isLeft = dragInfo.offsetX < event.target.offsetWidth / 2
    dragInfo.ctrl = event.ctrlKey
    dragInfo.targetId = textToColumnId[event.target.innerText]
    dragInfo.targetIndex = meta.colOrder.findIndex(a => a === dragInfo.targetId)
    // console.log('ondrop - dragInfo', dragInfo)
    if (dragInfo.sourceId === dragInfo.targetId) {
      dragEvent.alignEnd(dragInfo) // 在同一个dom里拖拽，释放
    } else {
      dragEvent.dragEnd(dragInfo) // 拖拽到另一个 dom，释放
    }
  }
}

export default setDrag
