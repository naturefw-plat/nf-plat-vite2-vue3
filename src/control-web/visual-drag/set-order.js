
/**
 * table的拖拽管理，可以调整th、td的顺序。
 * * 调整顺序
 * * 交换位置
 */
const setOrder = (meta, dragInfo) => {
  /**
   * 交换两个th的位置
   */
  const _swapPlaces = () => {
    // 交换
    meta.colOrder[dragInfo.sourceIndex] = dragInfo.targetId
    meta.colOrder[dragInfo.targetIndex] = dragInfo.sourceId
  }

  /**
   * 插入 th 后调整顺序
   */
  const _order = () => {
    // 判断前插、后插。后插：偏移 0；前插：偏移 1
    const offsetTarget = dragInfo.isLeft ? 0 : 1
    // 判断前后顺序。》 1；《 0
    const offsetSource = dragInfo.sourceIndex < dragInfo.targetIndex ? 0 : 1

    // 插入源
    meta.colOrder.splice(dragInfo.targetIndex + offsetTarget, 0, dragInfo.sourceId)
    // 删除源
    meta.colOrder.splice(dragInfo.sourceIndex + offsetSource, 1)
  }

  // 源和目标不同，排序
  if (dragInfo.ctrl) {
    // 交换
    _swapPlaces()
  } else {
    // 插入
    _order()
  }
}

export default setOrder
