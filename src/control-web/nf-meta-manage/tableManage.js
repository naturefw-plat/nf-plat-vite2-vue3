
import { nextTick } from 'vue'

/**
 * table的拖拽管理，可以调整th的顺序和对齐方式。
 * * 插入th
 * * 交互th的位置
 * * 设置th的对齐方式
 * * 设置td的对齐方式
 */
const manageTable = (girdMeta, dragInfo, tableInfo, tableClass) => {
  /**
   * 交换两个th的位置
   */
  const _changeSet = (cols) => {
    // console.log('========开始交换==============')
    // 交换
    girdMeta.colOrder[dragInfo.sourceIndex - 1] = cols.id2
    girdMeta.colOrder[dragInfo.targetIndex - 1] = cols.id1
    nextTick(() => {
      // table被重置了，所以需要重新加拖拽事件，重新排序。
      tableInfo.setThforDrag(tableClass, dragInfo)
    })
  }

  /**
   * 插入 th 后调整顺序
   */
  const _order = (cols) => {
    // 判断前插、后插。后插：偏移 0；前插：偏移 1
    const offsetTarget = dragInfo.isLeft ? 1 : 0

    // 判断前后顺序。》 1；《 0
    const offsetSource = dragInfo.sourceIndex < dragInfo.targetIndex ? 1 : 0

    // 插入源
    girdMeta.colOrder.splice(dragInfo.targetIndex - offsetTarget, 0, cols.id1)
    // 删除源
    girdMeta.colOrder.splice(dragInfo.sourceIndex - offsetSource, 1)
    console.log('新的拖拽函数111')
    nextTick(() => {
      // table被重置了，所以需要重新加拖拽事件，重新排序。
      tableInfo.setThforDrag(tableClass, dragInfo)
    })
  }

  /**
   * 设置th的对齐方式
   */
  const _setThAlgin = (cols) => {
    // 判断 th 还是 td
    const alignKind = (dragInfo.ctrl) ? 'header-align' : 'align'
    const col = girdMeta.itemMeta[cols.id1]
    // 判断：左中右
    switch (col[alignKind]) {
      case 'left':
        if (dragInfo.isLeft) {
          col[alignKind] = 'left'
        } else {
          col[alignKind] = 'center'
        }
        break
      case 'center':
        if (dragInfo.isLeft) {
          col[alignKind] = 'left'
        } else {
          col[alignKind] = 'right'
        }
        break
      case 'right':
        if (dragInfo.isLeft) {
          col[alignKind] = 'center'
        } else {
          col[alignKind] = 'right'
        }
        break
    }
  }

  /**
   * 设置th的顺序
   */
  const setThOrder = () => {
    // 获取colId
    // 判断前后顺序
    // 判断：前插、后插、交换。
    // 重新设置顺序
    // 获取colId
    const cols = {
      id1: girdMeta.colOrder[dragInfo.sourceIndex - 1],
      id2: girdMeta.colOrder[dragInfo.targetIndex - 1]
    }

    if (dragInfo.sourceIndex !== dragInfo.targetIndex) {
      // 源和目标不同，排序
      if (dragInfo.ctrl) {
        // 交换
        _changeSet(cols)
      } else {
        // 插入
        _order(cols)
      }
    } else {
      // 源和目标相同，设置对齐方式
      _setThAlgin(cols)
    }
  }

  return {
    setThOrder
  }
}

export default manageTable
