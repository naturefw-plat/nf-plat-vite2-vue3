
import { nextTick } from 'vue'

/**
 * form的拖拽管理，可以调整 字段 的顺序。
 * * 插入 字段
 * * 交互 字段 的位置
 */
const manageForm = (formMeta, dragInfo, tableInfo) => {
  /**
   * 交换两个th的位置
   */
  const _changeSet = (cols) => {
    // console.log('========开始交换==============')
    // 交换
    formMeta.colOrder[dragInfo.sourceIndex] = cols.id2
    formMeta.colOrder[dragInfo.targetIndex] = cols.id1
    // 强制响应
    formMeta.colOrder.push(cols.id1)
    nextTick(() => {
      // 删除多余的col
      formMeta.colOrder.splice(formMeta.colOrder.length - 1, 1)
      // table被重置了，所以需要重新加拖拽事件，重新排序。
      formMeta.events.reset()
      nextTick(() => {
        tableInfo.setFormforDrag(dragInfo)
      })
    })
  }

  /**
   * 插入 th 后调整顺序
   */
  const _order = (cols) => {
    // 判断前后顺序。》 1；《 0
    const offsetSource = dragInfo.sourceIndex < dragInfo.targetIndex ? 0 : -1

    // 判断前插、后插。后插：偏移 0；前插：偏移 1
    const offsetTarget = dragInfo.isLeft ? 0 : -1

    // 插入源
    formMeta.colOrder.splice(dragInfo.targetIndex - offsetTarget, 0, cols.id1)
    nextTick(() => {
      // 删除源
      formMeta.colOrder.splice(dragInfo.sourceIndex - offsetSource, 1)
      // table被重置了，所以需要重新加拖拽事件，重新排序。
      formMeta.events.reset()
      nextTick(() => {
        tableInfo.setFormforDrag(dragInfo)
      })
    })
  }

  /**
   * 设置表单的列数
   */
  const _setSpan0 = (cols) => {
    if (dragInfo.isLeft) {
      formMeta.formColCount -= 1
    } else {
      formMeta.formColCount += 1
    }
    if (formMeta.formColCount < 1) {
      formMeta.formColCount = 1
    }
    if (formMeta.formColCount > 12) {
      formMeta.formColCount = 12
    }
    formMeta.events.reset()
  }

  /**
   * 调整一个字段占几个 td，单列情况
   */
  const _setSpan1 = (cols) => {
    const col = formMeta.itemMeta[cols.id1]
    if (typeof col.colCount === 'undefined') {
      col.colCount = 1
    }
    if (dragInfo.isLeft) {
      col.colCount -= 1
    } else {
      col.colCount += 1
    }
    if (col.colCount > 1) col.colCount = 1
    if (col.colCount < -5) {
      col.colCount = -5
    }
    formMeta.events.reset()
  }

  /**
   * 调整一个字段占几个 td，多列情况
   */
  const _setSpan2 = (cols) => {
    const col = formMeta.itemMeta[cols.id1]
    if (typeof col.colCount === 'undefined') {
      col.colCount = 1
    }
    if (dragInfo.isLeft) {
      col.colCount -= 1
    } else {
      col.colCount += 1
    }

    if (col.colCount < 1) col.colCount = 1
    if (col.colCount > formMeta.formColCount) {
      col.colCount = formMeta.formColCount
    }
    formMeta.events.reset()
  }

  /**
   * 设置th的顺序
   */
  const setLabelOrder = () => {
    // 获取colId
    // 判断前后顺序
    // 判断：前插、后插、交换。
    // 重新设置顺序
    // 获取colId
    const cols = {
      id1: formMeta.colOrder[dragInfo.sourceIndex],
      id2: formMeta.colOrder[dragInfo.targetIndex]
    }

    if (dragInfo.sourceIndex !== dragInfo.targetIndex) {
      // 源和目标不同，排序
      if (dragInfo.ctrl) {
        // 交换
        _changeSet(cols)
      } else {
        // 插入
        _order(cols)
      }
    } else {
      if (dragInfo.sourceIndex === 0 && dragInfo.ctrl) {
        _setSpan0(cols)
      } else if (formMeta.formColCount === 1) {
        _setSpan1(cols)
      } else {
        _setSpan2(cols)
      }
    }
  }

  return {
    setLabelOrder
  }
}

export default manageForm
