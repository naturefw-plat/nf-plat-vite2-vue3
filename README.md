# nf-plat-vite2-vue3
文档驱动的支撑平台，用于维护各种meta。

#### 介绍
vite2 + vue3 做的支撑平台，前端部分。
实现表、字段的管理。
模块、表单、列表、查询、分页等的管理。
角色权限的管理。
共用模块的实现。

#### 软件架构
Vite2
Vue3
vue-router：路由
vue-data-state：状态管理
element-plus：UI库
nf-web-storage：前端数据库的访问
axios：访问后端
eslint：格式检查



#### 安装教程

1.  yarn

#### 使用说明


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
