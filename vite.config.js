import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path' // 主要用于alias文件路径别名

// 测试插件
import vitePluginVueTest from './plugin/test.js'

const pathResolve = (dir) => resolve(__dirname, '.', dir)

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vitePluginVueTest()],
  devtools: true,
  resolve: {
    alias: {
      '/@': resolve(__dirname, '.', 'src'),
      // service 相关
      '/services': pathResolve('src/service/server.js'), // service.js
      '/service-geturl': pathResolve('src/service/geturl.js'), // service.js
      // '/service-reloadMeta': pathResolve('src/service/indexedDB-reload-meta.js'), // service.js
      // '/service-loadMeta': pathResolve('src/service/indexedDB-load-meta.js'), // service.js
      // 状态
      // '/nf-state': pathResolve('packages/state.js'),
      // meta的管理
      // '/nf-meta': pathResolve('packages/meta.js'),
      // 前端存储
      // '/nf-web-storage': pathResolve('packages/storage.js'),
      // 数据库
      // '/nf-webSQL': pathResolve('packages/websql.js'),
      '/config': pathResolve('public/config'),
      '/json': pathResolve('public/json'),
      '/jsonProject': pathResolve('public/json-project'),
      '/jsonPlat': pathResolve('public/json-plat'),
      '/jsonPlatAPI': pathResolve('public/json-plat-api'),
      '/jsonCRUD': pathResolve('public/json-crud'),
      '/jsonAPI': pathResolve('public/json-api'),
      // '/com': pathResolve('src/components'),
      '/comps': pathResolve('src/components'),
      '/ctrl': pathResolve('src/control-web'),
      '/nf-control-web': pathResolve('packages/index.js'),
      '/views': pathResolve('src/views')
    }
  },
  // base: 'nf-plat-vite2-vue3', // 便于发布到gitee
  // base: 'nf-vue-cdn/elecontrol', // 发布到gitee、cdn
  // 打包配置
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
        project: resolve(__dirname, 'project/index.html')
      }
    },
    target: 'modules',
    // outDir: 'dist', //指定输出路径
    // assetsDir: 'assets', // 指定生成静态资源的存放路径
    minify: 'terser' // 混淆器，terser构建后文件体积更小
  },
  // 本地运行配置，及反向代理配置
  server: {
    cors: true, // 默认启用并允许任何源
    open: false, // 在服务器启动时自动在浏览器中打开应用程序
    // 反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
    proxy: {
      '/api': {
        target: 'http://localhost:6000/', // 代理接口
        changeOrigin: true
        // rewrite: (path) => path // .replace(/^\/api/, '')
      }
    }
  }
})
