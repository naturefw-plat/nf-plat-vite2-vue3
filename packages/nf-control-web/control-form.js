import { reactive, watch, unref } from 'vue'
import { createModel } from './control-model'

/**
 * 设置表单的各种 model
 * @param { object } props 表单组件的属性
 * @returns
 */
const setFormModel = (props, context) => {
  // 设置部分 model 和精简 model
  const setPartModel = (controlId, colName) => {
    // 同步到部分model
    if (typeof props.partModel[colName] !== 'undefined') {
      props.partModel[colName] = props.modelValue[colName]
    }
  }

  // 向父组件提交 model
  const myChange = (val, controlId, colName) => {
    setPartModel(controlId, colName)
    // 触发父组件的事件
    context.emit('my-change', val, controlId, colName)
  }

  // 依据用户选项，创建对应的 model
  const createPartModel = (array) => {
    // 表单子控件的属性
    const formItemProps = props.itemMeta

    // 删除内部局部 model 的 属性
    for (const key in props.partModel) {
      delete props.partModel[key]
    }
    // 删除外部局部 model 的 属性
    for (const key in props.partModel) {
      delete props.partModel[key]
    }

    // 建立新属性
    for (let i = 0; i < array.length; i++) {
      const colName = formItemProps[array[i]].colName
      props.partModel[colName] = props.modelValue[colName]
    }
    for (let i = 0; i < array.length; i++) {
      const colName = formItemProps[array[i]].colName
      const value = props.modelValue[colName]
      const defValue = formItemProps[array[i]].defValue
    }
  }

  return {
    setPartModel,
    myChange,
    createPartModel
  }
}

/**
 * 处理一个字段占用几个td的需求
 * @param { object } props 表单组件的属性
 * @returns
 */
const getColSpan = (props) => {
  // 确定一个组件占用几个格子
  const formColSpan = reactive({})

  // 根据配置里面的colCount，设置 formColSpan
  const setFormColSpan = () => {
    const formColCount = props.formColCount // 列数
    const moreColSpan = 24 / formColCount // 一个格子占多少份
    // 表单子控件的属性
    const formItemProps = props.itemMeta

    if (formColCount === 1) {
      // 一列的情况
      for (const key in formItemProps) {
        const m = formItemProps[key]
        if (typeof m.colCount === 'undefined') {
          formColSpan[m.controlId] = moreColSpan
        } else {
          if (m.colCount >= 1) {
            // 单列，多占的也只有24格
            formColSpan[m.controlId] = moreColSpan
          } else if (m.colCount < 0) {
            // 挤一挤的情况， 24 除以 占的份数
            formColSpan[m.controlId] = moreColSpan / (0 - m.colCount)
          }
        }
      }
    } else {
      // 多列的情况
      for (const key in formItemProps) {
        const m = formItemProps[key]
        if (typeof m.colCount === 'undefined') {
          formColSpan[m.controlId] = moreColSpan
        } else {
          if (m.colCount < 0 || m.colCount === 1) {
            // 多列，挤一挤的占一份
            formColSpan[m.controlId] = moreColSpan
          } else if (m.colCount > 1) {
            // 多列，占的格子数 * 份数
            formColSpan[m.controlId] = moreColSpan * m.colCount
          }
        }
      }
    }
  }

  return {
    formColSpan,
    setFormColSpan
  }
}

/**
 * 处理字段排序的需求
 * @param { object } props 表单组件的属性
 * @returns
 */
const getColSort = (props) => {
  // 定义排序依据
  const formColSort = reactive([])

  // 设置组件的显示顺序
  const setFormColSort = (array = props.colOrder) => {
    formColSort.length = 0
    formColSort.push(...array)
  }
  // 先运行一下
  setFormColSort()

  return {
    formColSort,
    setFormColSort
  }
}

/**
 * 创建表单的验证规则
 * @param { object } props 表单组件的属性
 * @returns
 */
const getRules = (props) => {
  const rules = {}

  // 表单子控件的属性
  const itemMeta = props.itemMeta

  for (const key in props.ruleMeta) {
    const rule = props.ruleMeta[key]
    rules[itemMeta[key].colName] = rule
  }

  return rules
}

/**
 * @function 表单控件的管理类
 * @description 创建 v-model，创建局部model，设置行列、排序相关的处理
 * @param { object } props 组件参数
 * @param { object } context 上下文
 * @returns { function } 表单管理类
 * * formModel 表单v-model
 * * 创建v-model
 * * 调整列数
 * * 合并
 */
export default function controlForm (props, context) {
  // 设置 modelValue 的 属性
  if (Object.keys(props.modelValue).length === 0) {
    // 根据子控件的meta，设置默认的model
    Object.assign(props.modelValue, createModel(props.itemMeta))
  }

  // 设置表单的各种model
  const {
    setPartModel,
    myChange,
    createPartModel
  } = setFormModel(props, context)

  // 关于一个字段占用几个td的问题
  const {
    formColSpan, // 一个字段占几个td的对象
    setFormColSpan // 设置的函数
  } = getColSpan(props)

  // 先运行一次
  setFormColSpan()

  // 关于字段显示顺序的文
  const {
    formColSort,
    setFormColSort
  } = getColSort(props)

  const setFormColShow = () => {
    // 数据变化，联动组件的显示
    if (typeof props.formColShow !== 'undefined') {
      for (const key in props.formColShow) {
        const ctl = props.formColShow[key]
        const colName = props.itemMeta[key].colName
        // 监听组件的值，有变化就重新设置局部model
        watch(() => props.modelValue[colName], (v1, v2) => {
          if (typeof ctl[v1] === 'undefined') {
            // 没有设定，显示默认组件
            setFormColSort()
          } else {
            // 按照设定显示组件
            setFormColSort(ctl[v1])
            // 设置部分的 model
            createPartModel(ctl[v1])
          }
        },
        { immediate: true })
      }
    }
  }
  setFormColShow()

  const rules = getRules(props)

  // 返回重置函数
  props.events.reset = () => {
    // alert('表单内部修改外部函数')
    setFormColSpan()
    setFormColSort()
    setFormColShow()
  }

  return {
    // 对象
    // formModel, // 完整的 v-model
    // formPartModel, // 用户选择的组件的 model
    formColSpan, // 确定组件占位
    formColSort, // 确定组件排序
    formRules: rules, // 表单的验证规则
    // 函数
    setFormColSpan, // 设置组件占位
    setFormColSort, // 设置组件排序
    setFormColShow, // 设置组件联动
    myChange // 提交
  }
}
